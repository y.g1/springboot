package com.ts.Empdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpdataApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpdataApplication.class, args);
	}

}
